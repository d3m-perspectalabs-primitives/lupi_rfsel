from typing import Any, Callable, List, Dict, Union, Optional, Sequence, Tuple
from numpy import ndarray
from collections import OrderedDict
from scipy import sparse
import os
import math
from sklearn.impute import SimpleImputer as Imputer

import sklearn
import numpy
import typing

import pandas as pd
import numpy as np

from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.ensemble.forest import RandomForestClassifier

from sklearn.kernel_ridge import KernelRidge
from sklearn import preprocessing
from sklearn.utils.validation import check_X_y, check_array
from sklearn.utils.validation import indexable, check_is_fitted
from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m import utils
from d3m.base import utils as base_utils
from d3m.exceptions import PrimitiveNotFittedError
from d3m.primitive_interfaces.base import CallResult, DockerContainer

from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
import common_primitives.utils as common_utils

from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin
from d3m import exceptions
import pandas
import sys
import logging

log = logging.getLogger(__name__)
import warnings
warnings.filterwarnings("ignore")


Inputs = d3m_dataframe
Outputs = d3m_dataframe


class Params(params.Params):
    estimators_: Optional[List[sklearn.tree.DecisionTreeClassifier]]
    classes_: Optional[Union[ndarray, List[ndarray]]]
    n_classes_: Optional[Union[int, List[int]]]
    n_features_: Optional[int]
    n_outputs_: Optional[int]
    oob_score_: Optional[float]
    oob_decision_function_: Optional[ndarray]
    base_estimator_: Optional[object]
    estimator_params: Optional[tuple]
    base_estimator: Optional[object]
    input_column_names: Optional[Any]
    target_names_: Optional[Sequence[Any]]
    training_indices_: Optional[Sequence[int]]
    target_column_indices_: Optional[Sequence[int]]
    target_columns_metadata_: Optional[List[OrderedDict]]

class Hyperparams(hyperparams.Hyperparams):
    n_estimators = hyperparams.Bounded[int](
        default=5000,
        lower=1,
        upper=None,
        description='The number of trees in the forest.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    criterion = hyperparams.Enumeration[str](
        values=['gini', 'entropy'],
        default='gini',
        description='The function to measure the quality of a split. Supported criteria are "gini" for the Gini impurity and "entropy" for the information gain. Note: this parameter is tree-specific.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    max_features = hyperparams.Union(
        configuration=OrderedDict({
            'specified_int': hyperparams.Bounded[int](
                lower=0,
                upper=None,
                default=0,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'calculated': hyperparams.Enumeration[str](
                values=['auto', 'sqrt', 'log2'],
                default='auto',
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'none': hyperparams.Constant(
                default=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'percent': hyperparams.Uniform(
                default=0.25,
                lower=0,
                upper=1,
                lower_inclusive=True,
                upper_inclusive=False,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='calculated',
        description='The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If "auto", then `max_features=sqrt(n_features)`. - If "sqrt", then `max_features=sqrt(n_features)` (same as "auto"). - If "log2", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    max_depth = hyperparams.Union(
        configuration=OrderedDict({
            'int': hyperparams.Bounded[int](
                default=10,
                lower=0,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'none': hyperparams.Constant(
                default=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='int',
        description='The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    min_samples_split = hyperparams.Union(
        configuration=OrderedDict({
            'absolute': hyperparams.Bounded[int](
                default=2,
                lower=1,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'percent': hyperparams.Uniform(
                default=0.25,
                lower=0,
                upper=1,
                lower_inclusive=False,
                upper_inclusive=True,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='absolute',
        description='The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    min_samples_leaf = hyperparams.Union(
        configuration=OrderedDict({
            'absolute': hyperparams.Bounded[int](
                default=1,
                lower=1,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'percent': hyperparams.Uniform(
                default=0.25,
                lower=0,
                upper=0.5,
                lower_inclusive=False,
                upper_inclusive=True,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='absolute',
        description='The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    min_weight_fraction_leaf = hyperparams.Uniform(
        default=0,
        lower=0,
        upper=0.5,
        upper_inclusive=True,
        description='The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    max_leaf_nodes = hyperparams.Union(
        configuration=OrderedDict({
            'int': hyperparams.Bounded[int](
                default=10,
                lower=0,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'none': hyperparams.Constant(
                default=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='none',
        description='Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    min_impurity_decrease = hyperparams.Bounded[float](
        default=0.0,
        lower=0.0,
        upper=None,
        description='A node will be split if this split induces a decrease of the impurity greater than or equal to this value.  The weighted impurity decrease equation is the following::  N_t / N * (impurity - N_t_R / N_t * right_impurity - N_t_L / N_t * left_impurity)  where ``N`` is the total number of samples, ``N_t`` is the number of samples at the current node, ``N_t_L`` is the number of samples in the left child, and ``N_t_R`` is the number of samples in the right child.  ``N``, ``N_t``, ``N_t_R`` and ``N_t_L`` all refer to the weighted sum, if ``sample_weight`` is passed.  .. versionadded:: 0.19 ',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    bootstrap = hyperparams.UniformBool(
        default=True,
        description='Whether bootstrap samples are used when building trees.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    oob_score = hyperparams.UniformBool(
        default=False,
        description='Whether to use out-of-bag samples to estimate the generalization accuracy.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    n_jobs = hyperparams.Union(
        configuration=OrderedDict({
            'limit': hyperparams.Bounded[int](
                default=4,
                lower=1,
                upper=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'all_cores': hyperparams.Constant(
                default=-1,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='limit',
        description='The number of jobs to run in parallel for both `fit` and `predict`. If -1, then the number of jobs is set to the number of cores.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter']
    )
    warm_start = hyperparams.UniformBool(
        default=False,
        description='When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new forest.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    class_weight = hyperparams.Union(
        configuration=OrderedDict({
            'str': hyperparams.Enumeration[str](
                default='balanced',
                values=['balanced', 'balanced_subsample'],
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'none': hyperparams.Constant(
                default=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='str',
        description='"balanced_subsample" or None, optional (default=None) Weights associated with classes in the form ``{class_label: weight}``. If not given, all classes are supposed to have weight one. For multi-output problems, a list of dicts can be provided in the same order as the columns of y.  The "balanced" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``  The "balanced_subsample" mode is the same as "balanced" except that weights are computed based on the bootstrap sample for every tree grown.  For multi-output, the weights of each column of y will be multiplied.  Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    
    use_input_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
    )
    use_output_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_input_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
    )
    exclude_output_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
    )
    add_index_columns = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    error_on_no_input = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False.",
    )
    regressor_type = hyperparams.Enumeration[str](
        semantic_types=["https://metadata.datadrivendiscovery.org/types/TuningParameter"],
        values=['linear', 'kernelridge', 'none', 'all'],
        default='linear',
        description='Specifies the regression type.'
    )

    n_cv = hyperparams.Union(
        configuration=OrderedDict({
            'int': hyperparams.Bounded[int](
                default=5,
                lower=3,
                upper=7,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            ),
            'none': hyperparams.Constant(
                default=None,
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )
        }),
        default='int',
        description='The number of cross validation folders.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )

class LupiRFSelClassifier(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams],
                          ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    This is an implementation of the LUPI with RandomForest primitive and feature selection to solve classification problems. It can solve dataset problem with lupi features. If no lupi features presented, it can also learn new features from training data. 
    The values of the features in dataset need to be numerical, the label classes to be predicted should be categorical,
    and the missing values need to be imputed.
    
    """
    __git_commit__ = utils.current_git_commit(os.path.dirname(__file__))
    __author__ = "VencoreLabs"
    metadata = metadata_base.PrimitiveMetadata({ 
         "algorithm_types": [metadata_base.PrimitiveAlgorithmType.RANDOM_FOREST, ],
         "name": "lupi_rfsel.LupiRFSelClassifier",
         "primitive_family": metadata_base.PrimitiveFamily.CLASSIFICATION,
         "python_path": "d3m.primitives.classification.lupi_rfsel.LupiRFSelClassifier",
         "source": {'name': 'VencoreLabs',
                    'contact': 'mailto:plin@perspectalabs.com',
                    'uris': ['https://gitlab.com/d3m-perspectalabs-primitives/lupi_rfsel/blob/master/lupi_rfsel/lupi_rfsel.py',
                     'https://gitlab.com/d3m-perspectalabs-primitives/lupi_rfsel.git'
]},
         "version": "v6.0.0",
         "id": "5bb6d960-8b6d-4d90-aa8d-88bb9994a20a",
         'installation': [
                        {'type': metadata_base.PrimitiveInstallationType.PIP,
                           'package_uri': 'git+https://gitlab.com/d3m-perspectalabs-primitives/lupi_rfsel.git@{git_commit}#egg=lupi_rfsel'.format(
                            git_commit=__git_commit__,
                            ),
                           }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None,
                 _verbose: int = 0) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = RandomForestClassifier(
            n_estimators=self.hyperparams['n_estimators'],
            criterion=self.hyperparams['criterion'],
            max_features=self.hyperparams['max_features'],
            max_depth=self.hyperparams['max_depth'],
            min_samples_split=self.hyperparams['min_samples_split'],
            min_samples_leaf=self.hyperparams['min_samples_leaf'],
            min_weight_fraction_leaf=self.hyperparams['min_weight_fraction_leaf'],
            max_leaf_nodes=self.hyperparams['max_leaf_nodes'],
            min_impurity_decrease=self.hyperparams['min_impurity_decrease'],
            bootstrap=self.hyperparams['bootstrap'],
            oob_score=self.hyperparams['oob_score'],
            n_jobs=self.hyperparams['n_jobs'],
            warm_start=self.hyperparams['warm_start'],
            class_weight=self.hyperparams['class_weight'],
            random_state=self.random_seed,
            verbose=_verbose
        )
        
        self._training_inputs = None
        self._training_outputs = None
        self._target_names = None
        self._training_indices = None
        self._target_column_indices = None
        self._target_columns_metadata: List[OrderedDict] = None
        self._input_column_names = None
        self._n_estimators = self.hyperparams['n_estimators']
        self._cv_score = "accuracy"
        self._regressor_type = self.hyperparams['regressor_type']
        self._n_cv = self.hyperparams['n_cv']
        self._nJobs = hyperparams['n_jobs']
        self._fitted = False
        
    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        privileged_features = self._get_privileged_features(inputs)

        training_inputs_orig, self._training_indices = self._get_columns_to_fit(inputs, self.hyperparams)

        (trainData_df, targetCatLabel) = self._data_prep(training_inputs_orig, privileged_features)

        trainData_df.metadata = trainData_df.metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': d3m_dataframe,
            'privileged_features': privileged_features,
            'trainTargetsCatLabel': targetCatLabel
        })

        trainTargetsCatLabel = trainData_df.metadata.query(()).get('trainTargetsCatLabel')

        training_inputs = trainData_df.copy()

        training_inputs.drop(['d3mIndex'], axis=1, inplace=True)
        training_inputs.drop([trainTargetsCatLabel], axis=1, inplace=True)
        self._training_inputs = training_inputs
        self.privileged_features = trainData_df.metadata.query(()).get('privileged_features')

        self._training_outputs, self._target_names, self._target_column_indices = self._get_targets(outputs,
                                                                                                    self.hyperparams)
        self._input_column_names = self._training_inputs.columns
        self._fitted = False

    def _get_privileged_features(self, inputs):
        privileged_features = []
        privileged_semantic_type = 'https://metadata.datadrivendiscovery.org/types/PrivilegedData'
        col_length = inputs.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        for col in range(col_length):
            semantic_types = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col))['semantic_types']
            col_name = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col))['name']
            if privileged_semantic_type in semantic_types:
                #                print("col {} {} is privileged".format(col, col_name))
                privileged_features.append(col_name)

        return privileged_features

    def _data_imputer(self, inputs):
        inputData = inputs.copy()
        df = inputData.apply(pd.to_numeric, args=('coerce',))
        isallnull = df.isnull().all()
        allnull_columns = isallnull.index[isallnull].tolist()
        # set all nan columns to value 0 so that the whole column will not be imputed
        df.loc[:, allnull_columns] = 0
        imputer = Imputer(missing_values=np.nan, strategy='mean')
        imputer.fit(df)
        imputed_df = pd.DataFrame(imputer.transform(df))
        imputed_df.columns = df.columns
        imputed_df.index = df.index
        imputed_df['d3mIndex'] = imputed_df['d3mIndex'].astype(int)
        return imputed_df

    def _data_prep(self, inputs, privileged_features):
        inputData = self._data_imputer(inputs)
        inputData = inputData.fillna('0').replace('', '0')
        inputData = d3m_dataframe(inputData)

        categoricaldata_semantic_type = "https://metadata.datadrivendiscovery.org/types/CategoricalData"
        attribute_semantic_type = "https://metadata.datadrivendiscovery.org/types/Attribute"
        suggestedtarget_semantic_type = "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"

        col_length = inputData.shape[1]
        for col_index in range(col_length):
            col_metadata = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col_index))
            name = col_metadata['name']
            semantic_types = col_metadata['semantic_types']

            if categoricaldata_semantic_type in semantic_types and suggestedtarget_semantic_type in semantic_types:
                targetCatLabel = name
                #self._target_names = targetCatLabel
        return (inputData, targetCatLabel)

    def _lupi_fit(self, pri_features, regr_param_grid, rf_param_grid, nJobs=1):
        """
        fit with lupi features
        :param pri_features:
        :param regr_param_grid:
        :param rf_param_grid:
        :param nJobs:
        :return:
        """
        pri_features = list(pri_features)

        lupiregressor = LupiRegressor(pri_features=pri_features, nJobs=nJobs, regr_type_list=['linear'],
                                      regr_param_grid=regr_param_grid, cv=6)

        lupiregressor.fit(self._training_inputs, self._training_outputs)  # fit to get the lupi regrs from the train data

        self._lupiregressor = lupiregressor

        X_train = self._training_inputs.copy()
        X_train_lupilearned = lupiregressor.transform(X_train)
        X_train_transfered = X_train_lupilearned

        gs_estimator = GridSearchCV(self._clf, verbose=False, scoring=self._cv_score,
                                    cv=6, n_jobs=nJobs, param_grid=rf_param_grid)

        y_train = self._sk_training_output
        y_train = y_train.astype(int)
        gs_estimator.fit(X_train_transfered, y_train)
        self._gs_estimator = gs_estimator
        self._fitted = True

    def _sel_fit(self, regr_param_grid, rf_param_grid, nJobs=1):
        """
        fit with selection features
        :param regr_param_grid:
        :param rf_param_grid:
        :param nJobs:
        :return:
        """
        regr_type = self._regressor_type
        X_train = self._training_inputs
        Y_train = self._training_outputs

        if regr_type == 'linear':
            selregressor = SelRegressor(regressor_type = 'linear',
                                        regr_param_grid=regr_param_grid,
                                        nJobs=nJobs,
                                        cv=self._n_cv)
            selregressor.fit(X_train,
                             Y_train)  # fit to get the lupi regrs from the train data
            X_train_learned = selregressor.transform(X_train)
        elif regr_type == 'kernelridge':
            selregressor = SelRegressor(regressor_type = 'kernelridge',
                                        regr_param_grid=regr_param_grid,
                                        nJobs=nJobs,
                                        cv=self._n_cv)
            selregressor.fit(X_train,
                             Y_train)  # fit to get the lupi regrs from the train data
            X_train_learned = selregressor.transform(X_train)
        elif regr_type == 'none':
            X_train_learned = X_train
            selregressor = None
        else:
            log.error("regr_type is not valid")
            sys.exit(1)

        self._selregressor = selregressor

        gs_estimator = GridSearchCV(self._clf, verbose=False, scoring=self._cv_score,
                                    cv=self._n_cv, n_jobs=nJobs, param_grid=rf_param_grid)

        y_train = self._sk_training_output
        y_train = y_train.astype(int)
        gs_estimator.fit(X_train_learned, y_train)
        self._gs_estimator = gs_estimator
        self._fitted = True


    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        nJobs = self._nJobs
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        if len(self._training_indices) > 0 and len(self._target_column_indices) > 0:
            self._target_columns_metadata = self._get_target_columns_metadata(self._training_outputs.metadata)
            self._sk_training_output = self._training_outputs.values

            shape = self._sk_training_output.shape
            if len(shape) == 2 and shape[1] == 1:
                self._sk_training_output = numpy.ravel(self._sk_training_output)

            pri_features_names = self.privileged_features
            pri_features = [self._training_inputs.columns.get_loc(x) for x in pri_features_names]

            #log.info("self._gamma_gridsearch {}".format(self._gamma_gridsearch))
            #log.info("self._C_gridsearch {}".format(self._C_gridsearch))

            regr_param_grid_dict = dict(gamma_list=[-1.0, 6.5, 4.4], C_list=[-1.0, 6.5, 4.4])
            C_list = regr_param_grid_dict['C_list']
            gamma_list = regr_param_grid_dict['gamma_list']
            C_range = 2.0 ** np.arange(C_list[0], C_list[1], C_list[2])
            gamma_range = 1.0 / (2 * ((2 ** np.arange(gamma_list[0], gamma_list[1], gamma_list[2])) ** 2))
            alpha_range = [1. / (2. * c) for c in C_range]
            regr_param_grid = dict(alpha=alpha_range, gamma=gamma_range)

            # Number of trees in random forest
            start = 3000
            stop = self._n_estimators
            if (stop - start) > 10 :
                n_estimators = [int(x) for x in np.linspace(start=start, stop=stop, num=2)]
            else:
                n_estimators = [ self._n_estimators ]
            # Number of features to consider at every split
            max_features = ['auto', 'sqrt']
            # Maximum number of levels in tree
            max_depth = [int(x) for x in np.linspace(10, 30, num=2)]
            max_depth.append(None)
            # Minimum number of samples required to split a node
            min_samples_split = [2, 5, 10]
            # Minimum number of samples required at each leaf node
            min_samples_leaf = [1, 2, 4]
            # Method of selecting samples for training each tree
            bootstrap = [True, False]
            # Create the random grid
            rf_param_grid = {'n_estimators': n_estimators,
                             'max_depth': max_depth,
                             'bootstrap': bootstrap}
            #rf_param_grid = {'n_estimators': n_estimators,
                           #'max_features': max_features,
                           #'max_depth': max_depth,
                           #'min_samples_split': min_samples_split,
                           #'min_samples_leaf': min_samples_leaf,
                           #'bootstrap': bootstrap}

            if len(pri_features) > 0:  # dataset has privileged features
                self._lupi_fit(pri_features, regr_param_grid, rf_param_grid, nJobs=nJobs)
            else:  # datasett has no privileged feature
                self._sel_fit(regr_param_grid, rf_param_grid, nJobs=nJobs)

            self._fitted = True
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warning("No input columns were selected")

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        inputData = self._data_imputer(inputs)
        inputData = d3m_dataframe(inputData)
        inputData = inputData.fillna('0').replace('', '0')

        targetCatLabel = None

        categoricaldata_semantic_type = "https://metadata.datadrivendiscovery.org/types/CategoricalData"
        attribute_semantic_type = "https://metadata.datadrivendiscovery.org/types/Attribute"
        suggestedtarget_semantic_type = "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"

        col_length = inputData.shape[1]
        for col_index in range(col_length):
            col_metadata = inputs.metadata.query((metadata_base.ALL_ELEMENTS, col_index))
            name = col_metadata['name']
            structural_type = col_metadata['structural_type']
            semantic_types = col_metadata['semantic_types']
            if categoricaldata_semantic_type in semantic_types and suggestedtarget_semantic_type in semantic_types:
                targetCatLabel = name

        test_inputs = inputData.copy()
        privileged_features = self.privileged_features

        if targetCatLabel:
            test_inputs.drop([targetCatLabel], axis=1, inplace=True)

        test_inputs.drop(['d3mIndex'], axis=1, inplace=True)
        #test_inputs.drop(self.featureDrop, axis=1, inplace=True)
        #test_inputs.drop([privileged_features], axis=1, inplace=True)

        if len(privileged_features) > 0: # dataset has privileged features
            lupiregr = self._lupiregressor
            X_test_all = test_inputs.copy()
            X_test = X_test_all.drop(list(privileged_features), axis=1, inplace=False)

            X_test_lupilearned = lupiregr.transform(X_test)
            X_test_transfered = X_test_lupilearned
        else:  # datasett has no privileged feature
            selregr = self._selregressor
            X_test = test_inputs.copy()
            if selregr:
                X_test_transfered = selregr.transform(X_test)
            else:
                X_test_transfered = X_test # svm case
        prediction = self._gs_estimator.best_estimator_.predict(X_test_transfered)

        if sparse.issparse(prediction):
            prediction =  prediction.toarray()
        output = d3m_dataframe( prediction, generate_metadata=False)
        column_names = self._target_names
        output.columns = column_names
        output.metadata = inputs.metadata.clear(for_value=output, generate_metadata=True)
        #output.metadata = self._add_target_columns_metadata(outputs_metadata=output.metadata)
        output.metadata = self._add_target_semantic_types(metadata=output.metadata, target_names=self._target_names,
                                                          source=self)

        outputs = common_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=self._training_indices,
                                               columns_list=[output])
        return CallResult(outputs)


    def get_params(self) -> Params:
        if not self._fitted:
            return Params(
                estimators_=None,
                classes_=None,
                n_classes_=None,
                n_features_=None,
                n_outputs_=None,
                oob_score_=None,
                oob_decision_function_=None,
                base_estimator_=None,
                estimator_params=None,
                base_estimator=None,
                input_column_names=self._input_column_names,
                training_indices_=self._training_indices,
                target_names_=self._target_names,
                target_column_indices_=self._target_column_indices,
                target_columns_metadata_=self._target_columns_metadata
            )

        return Params(
            estimators_=getattr(self._clf, 'estimators_', None),
            classes_=getattr(self._clf, 'classes_', None),
            n_classes_=getattr(self._clf, 'n_classes_', None),
            n_features_=getattr(self._clf, 'n_features_', None),
            n_outputs_=getattr(self._clf, 'n_outputs_', None),
            oob_score_=getattr(self._clf, 'oob_score_', None),
            oob_decision_function_=getattr(self._clf, 'oob_decision_function_', None),
            base_estimator_=getattr(self._clf, 'base_estimator_', None),
            estimator_params=getattr(self._clf, 'estimator_params', None),
            base_estimator=getattr(self._clf, 'base_estimator', None),
            input_column_names=self._input_column_names,
            training_indices_=self._training_indices,
            target_names_=self._target_names,
            target_column_indices_=self._target_column_indices,
            target_columns_metadata_=self._target_columns_metadata
        )

    def set_params(self, *, params: Params) -> None:
        self._clf.estimators_ = params['estimators_']
        self._clf.classes_ = params['classes_']
        self._clf.n_classes_ = params['n_classes_']
        self._clf.n_features_ = params['n_features_']
        self._clf.n_outputs_ = params['n_outputs_']
        self._clf.oob_score_ = params['oob_score_']
        self._clf.oob_decision_function_ = params['oob_decision_function_']
        self._clf.base_estimator_ = params['base_estimator_']
        self._clf.estimator_params = params['estimator_params']
        self._clf.base_estimator = params['base_estimator']
        self._input_column_names = params['input_column_names']
        self._training_indices = params['training_indices_']
        self._target_names = params['target_names_']
        self._target_column_indices = params['target_column_indices_']
        self._target_columns_metadata = params['target_columns_metadata_']
        
        if params['estimators_'] is not None:
            self._fitted = True
        if params['classes_'] is not None:
            self._fitted = True
        if params['n_classes_'] is not None:
            self._fitted = True
        if params['n_features_'] is not None:
            self._fitted = True
        if params['n_outputs_'] is not None:
            self._fitted = True
        if params['oob_score_'] is not None:
            self._fitted = True
        if params['oob_decision_function_'] is not None:
            self._fitted = True
        if params['base_estimator_'] is not None:
            self._fitted = True
        if params['estimator_params'] is not None:
            self._fitted = True
        if params['base_estimator'] is not None:
            self._fitted = True


    def log_likelihoods(self, *,
                    outputs: Outputs,
                    inputs: Inputs,
                    timeout: float = None,
                    iterations: int = None) -> CallResult[Sequence[float]]:
        inputs = inputs.iloc[:, self._training_indices]  # Get ndarray
        outputs = outputs.iloc[:, self._target_column_indices]

        if len(inputs.columns) and len(outputs.columns):

            if outputs.shape[1] != self._clf.n_outputs_:
                raise exceptions.InvalidArgumentValueError("\"outputs\" argument does not have the correct number of target columns.")

            log_proba = self._clf.predict_log_proba(inputs)

            # Making it always a list, even when only one target.
            if self._clf.n_outputs_ == 1:
                log_proba = [log_proba]
                classes = [self._clf.classes_]
            else:
                classes = self._clf.classes_

            samples_length = inputs.shape[0]

            log_likelihoods = []
            for k in range(self._clf.n_outputs_):
                # We have to map each class to its internal (numerical) index used in the learner.
                # This allows "outputs" to contain string classes.
                outputs_column = outputs.iloc[:, k]
                classes_map = pandas.Series(numpy.arange(len(classes[k])), index=classes[k])
                mapped_outputs_column = outputs_column.map(classes_map)

                # For each target column (column in "outputs"), for each sample (row) we pick the log
                # likelihood for a given class.
                log_likelihoods.append(log_proba[k][numpy.arange(samples_length), mapped_outputs_column])

            results = d3m_dataframe(dict(enumerate(log_likelihoods)), generate_metadata=True)
            results.columns = outputs.columns

            for k in range(self._clf.n_outputs_):
                column_metadata = outputs.metadata.query_column(k)
                if 'name' in column_metadata:
                    results.metadata = results.metadata.update_column(k, {'name': column_metadata['name']})

        else:
            results = d3m_dataframe(generate_metadata=True)

        return CallResult(results)


    def produce_feature_importances(self) -> CallResult[d3m_dataframe]:
        output = d3m_dataframe(self._clf.feature_importances_.reshape((1, len(self._input_column_names))))
        output.columns = self._input_column_names
        for i in range(len(self._input_column_names)):
            output.metadata = output.metadata.update_column(i, {"name": self._input_column_names[i]})
        return CallResult(output)
    
    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return inputs, list(range(len(inputs.columns)))

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = base_utils.get_columns_to_use(inputs_metadata,
                                                                             use_columns=hyperparams['use_input_columns'],
                                                                             exclude_columns=hyperparams['exclude_input_columns'],
                                                                             can_use_column=can_produce_column)
        return inputs.iloc[:, columns_to_produce], columns_to_produce
        # return columns_to_produce

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = (int, float, numpy.integer, numpy.float64)
        accepted_semantic_types = set()
        accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/Attribute")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False
    
    @classmethod
    def _get_targets(cls, data: d3m_dataframe, hyperparams: Hyperparams):
        if not hyperparams['use_semantic_types']:
            return data, list(data.columns), list(range(len(data.columns)))

        metadata = data.metadata

        def can_produce_column(column_index: int) -> bool:
            accepted_semantic_types = set()
            accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/TrueTarget")
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            if len(semantic_types) == 0:
                cls.logger.warning("No semantic types found in column metadata")
                return False
            # Making sure all accepted_semantic_types are available in semantic_types
            if len(accepted_semantic_types - semantic_types) == 0:
                return True
            return False

        target_column_indices, target_columns_not_to_produce = base_utils.get_columns_to_use(metadata,
                                                                                               use_columns=hyperparams[
                                                                                                   'use_output_columns'],
                                                                                               exclude_columns=
                                                                                               hyperparams[
                                                                                                   'exclude_output_columns'],
                                                                                               can_use_column=can_produce_column)
        targets = []
        if target_column_indices:
            targets = data.select_columns(target_column_indices)
        target_column_names = []
        for idx in target_column_indices:
            target_column_names.append(data.columns[idx])
        return targets, target_column_names, target_column_indices

    @classmethod
    def _get_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata) -> List[OrderedDict]:
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict(outputs_metadata.query_column(column_index))

            # Update semantic types and prepare it for predicted targets.
            semantic_types = set(column_metadata.get('semantic_types', []))
            semantic_types_to_remove = set(["https://metadata.datadrivendiscovery.org/types/TrueTarget","https://metadata.datadrivendiscovery.org/types/SuggestedTarget",])
            add_semantic_types = set(["https://metadata.datadrivendiscovery.org/types/PredictedTarget",])
            semantic_types = semantic_types - semantic_types_to_remove
            semantic_types = semantic_types.union(add_semantic_types)
            column_metadata['semantic_types'] = list(semantic_types)

            target_columns_metadata.append(column_metadata)

        return target_columns_metadata
    
    @classmethod
    def _update_predictions_metadata(cls, inputs_metadata: metadata_base.DataMetadata, outputs: Optional[Outputs],
                                     target_columns_metadata: List[OrderedDict]) -> metadata_base.DataMetadata:
        outputs_metadata = metadata_base.DataMetadata().generate(value=outputs)

        for column_index, column_metadata in enumerate(target_columns_metadata):
            outputs_metadata = outputs_metadata.update_column(column_index, column_metadata)

        return outputs_metadata

    def _wrap_predictions(self, inputs: Inputs, predictions: ndarray) -> Outputs:
        outputs = d3m_dataframe(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(inputs.metadata, outputs, self._target_columns_metadata)
        return outputs


    @classmethod
    def _add_target_columns_metadata(cls, outputs_metadata: metadata_base.DataMetadata):
        outputs_length = outputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in range(outputs_length):
            column_metadata = OrderedDict()
            semantic_types = []
            semantic_types.append('https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            column_name = outputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index)).get("name")
            if column_name is None:
                column_name = "output_{}".format(column_index)
            column_metadata["semantic_types"] = semantic_types
            column_metadata["name"] = str(column_name)
            target_columns_metadata.append(column_metadata)

        return target_columns_metadata

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                                   source: typing.Any, target_names: List = None, ) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata

class LupiRegressor(BaseEstimator, TransformerMixin):
    """ A LUPI Regressor used to learn how to generate the privileged features from the standard features.
        Parameters
        ----------
        regr_type_list : list, list of elements consist of {'linear', 'ridge', 'svr'} ,  Default option is ['linear'].
            Choose the type of regression to be used when learning how to generate the privileged features.
        param_grid: the parameter search grid for kernel='rbf' regressors
        nJobs = int, optional , Default option is 1.
            Number of jobs to run in parallel during the training of the lupi regressor models.
        pri_features: a list of indices of privileged features in training dataset
        ----------
        input_shape : tuple
            The shape the data passed to :meth:`fit`
        """

    def __init__(self,  pri_features=[], regr_type_list=['linear'], regr_param_grid={}, nJobs=1, cv=6):
        self.pri_features = pri_features
        self.num_of_std = None
        self.cv = cv
        self.regr_list = []
        self.regr_type_list = regr_type_list
        self.regr_param_grid = regr_param_grid
        self.nJobs = nJobs
        self.input_shape_ = None

    def _prepare_x(self, X_train):  # prepare X for fit function
        pri_features = self.pri_features
        X = check_array(X_train)
        X_pri = X[:, pri_features]
        X_std = np.delete(X, np.s_[pri_features], axis=1)
        self.num_of_std = X_std.shape[1]
        pre_X = np.concatenate((X_std, X_pri), axis=1)
        return pre_X

    def fit(self, X_train, y_train):
        """Fit the model to data.
        Parameters
        ----------
        X_train : array-like or sparse matrix of shape = [n_samples, n_features],
            The training input samples consisting of both standard and privileged features.
        y_train : None
            There is no need of a target in a transformer, yet the pipeline API
            requires this parameter. y can be anything, it does not affect the result.

        Returns
        -------
        self : object
            Returns self.
        """
        pre_X = self._prepare_x(X_train)
        X_train = check_array(pre_X)
        self.input_shape_ = X_train.shape

        if self.pri_features == []:
            log.info("pri_features is empty!!")

        if self.num_of_std == 0:
            return self
        if not isinstance(X_train, pd.DataFrame):
            X_train = pd.DataFrame(X_train)

        num_of_std = self.num_of_std

        X = X_train.values  #convert pandas dataframe to numpy array
        X_stds = X[:, 0:num_of_std]

        supported_regr_types = ['kernelridge', 'linear']
        for regr_type in self.regr_type_list:
            if regr_type not in supported_regr_types:
                log.error("not supported regr_type{}".format(regr_type))
                sys.exit(1)

                ##############################
        feature_to_regrtype = {}  # dictionary mapping jth priv feature to its regrtype

        for j in np.arange(X.shape[1] - num_of_std):
            y_true_j = X[:, num_of_std + j]
            r2score_list = []  # list to store R2 values of regr_type_list
            regr_candidate_list = []  # list to store regr candidates for j th priv feature
            for regr_type in self.regr_type_list:
                if regr_type == 'kernelridge':
                    ridge_param_grid = self.regr_param_grid

                    regr = GridSearchCV(KernelRidge(kernel='rbf'),
                                        param_grid=ridge_param_grid,
                                        scoring='r2', cv=self.cv,
                                        n_jobs=self.nJobs)
                    regr.fit(X_stds, y_true_j)
                    best_cvscore = regr.best_score_
                    r2score_list.append(best_cvscore)
                    regr_candidate_list.append(regr)
                else:
                    regr = LinearRegression(fit_intercept=1)
                    regr.fit(X_stds, y_true_j)
                    r2_score = regr.score(X_stds, y_true_j)
                    r2score_list.append(r2_score)
                    regr_candidate_list.append(regr)
                    ##############################
            maxR2_index = r2score_list.index(max(r2score_list))  # index of the max R2 list
            best_regr = regr_candidate_list[maxR2_index]
            feature_to_regrtype[j] = self.regr_type_list[maxR2_index]
            self.regr_list.append(best_regr)

        self.feature_to_regrtype = feature_to_regrtype

        return self

    def transform(self, X, y=None):
        """ Transform the input data into standard and generated (reconstructed) privileged features.
            It transforms the data set according to the shape of the input dataset.
            The input data can be:
                1) data set with both standard and privileged features. This is the case for lupi training.
                2) data set with only standard features. This is the case for predicting of test data.
            Parameters
            ----------
            X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples which the features are in the original order. X will be pre-processed to
            pre_x which has the features re-ordered in that the first num_of_std features are standard and
            the next features are the privileged.

            y : numpy array of shape [n_samples]
                Target values.

            Returns
            -------
            X_transformed : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. The first num_of_std features are standard features
             and the next features are the reconstructed privileged features.
            """
        # Check is fit had been called
        check_is_fitted(self, ['input_shape_'])

        # Input validation
        num_of_std = self.num_of_std
        if self.num_of_std == 0:
            sys.exit("Unexpected num_of_std == 0")

        if X.shape[1] == self.input_shape_[1]:  # fit dataset with both standard and privileged features
            pre_X = self._prepare_x(X)
            X = check_array(pre_X)
            X2 = X.copy()
            for j in np.arange(len(self.regr_list)):
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2[:, 0:num_of_std])
        elif X.shape[1] == self.num_of_std:  # transfer test dataset with only standard features before lupi predict
            X = check_array(X)
            X2 = X.copy()
            lupi_feat = np.empty([X.shape[0], len(self.regr_list)])
            X2 = np.concatenate((X2, lupi_feat), axis=1)
            for j in np.arange(len(self.regr_list)):
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2[:, 0:num_of_std])
        else:
            log.error("Error: wrong input data dimension X.shape[1] = {}".format(X.shape[1]))
            sys.exit("Unexpected input data dimension of X")
        return X2

class SelRegressor(BaseEstimator, TransformerMixin):
    """ A feature augment and selection Regressor.
        Parameters
        ----------
        regr_type_list : list, list of elements consist of {'linear', 'ridge'} ,  Default option is ['linear'].
            Choose the type of regression to be used when learning how to generate the privileged features.
        param_grid: the parameter search grid for kernel='rbf' regressors
        nJobs = int, optional , Default option is 1.
            Number of jobs to run in parallel during the training of the lupi regressor models.
        pri_features: a list of indices of privileged features in training dataset
        ----------
        input_shape : tuple
            The shape the data passed to :meth:`fit`
        """

    def __init__(self, regressor_type='linear', regr_param_grid={}, nJobs=1, cv=6):
        self.num_of_std = None
        self.cv = cv
        self.regressor_type = regressor_type
        self.regr_param_grid = regr_param_grid
        self.nJobs = nJobs
        self.input_shape_ = None

    def _train_regr(self, X_train):
        self.input_shape_ = X_train.shape
        # total number of standard features
        num_of_std = X_train.shape[1]
        self.num_of_std = num_of_std
        X_std = X_train.values

        ## now, fit the lupi regressors
        regr_list_tmp = []
        #supported_regr_types = ['kernelridge','linear', 'none', 'all']
        supported_regr_types = ['kernelridge', 'linear']

        if self.regressor_type not in supported_regr_types:
            log.error("not supported regr_type{}".format(regr_type))
            sys.exit(1)

        regrs=[]
        for j in np.arange(num_of_std):
            y_true_j = X_std[:, j]
            X_stds_noj = np.delete(X_std, [j], axis = 1)

            if self.regressor_type == 'kernelridge':
                regr = GridSearchCV(KernelRidge(kernel='rbf'),
                                    param_grid=self.regr_param_grid,
                                    scoring='r2', cv=self.cv,
                                    n_jobs = self.nJobs)
                regr.fit(X_stds_noj, y_true_j)
                regrs.append(regr)
            elif self.regressor_type == 'linear':
                regr = LinearRegression(fit_intercept=1)
                regr.fit(X_stds_noj, y_true_j)
                regrs.append(regr)

        regr_list_tmp.extend(list(regrs))
        return regr_list_tmp


    def fit(self, X_train, y_train):
        trained_regrs = self._train_regr(X_train)
        log.info("regr_list lenght: {}".format(len(trained_regrs)))
        self.regr_list = trained_regrs
        return self

    def transform(self, X, y=None):
        """ Transform the input data into standard and generated (reconstructed) privileged features.
            It transforms the data set according to the shape of the input dataset.
            The input data can be:
                1) data set with both standard and privileged features. This is the case for lupi training.
                2) data set with only standard features. This is the case for predicting of test data.
            Parameters
            ----------
            X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples which the features are in the original order. X will be pre-processed to
            pre_x which has the features re-ordered in that the first num_of_std features are standard and
            the next features are the privileged.

            y : numpy array of shape [n_samples]
                Target values.

            Returns
            -------
            X_transformed : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. The first num_of_std features are standard features
             and the next features are the reconstructed privileged features.
            """
        # Check is fit had been called
        check_is_fitted(self, ['input_shape_'])

        # Input validation
        num_of_std = self.num_of_std
        if self.num_of_std == 0:
            sys.exit("Unexpected num_of_std == 0")
        if X.shape[1] == self.input_shape_[1]:
            X = check_array(X)
            X2 = X.copy()
            X2_o = X.copy()
            lupi_feat = np.empty([X.shape[0], len(self.regr_list)])
            X2 = np.concatenate((X2, lupi_feat), axis=1)

            for j in np.arange(len(self.regr_list)):
                X2_noj = np.delete(X2_o, [j], axis=1)
                X2[:, num_of_std + j] = self.regr_list[j].predict(X2_noj)
        else:
            log.error("Error: wrong input data dimension X.shape[1] = {}".format(X.shape[1]))
            sys.exit("Unexpected input data dimension of X")
        return X2
